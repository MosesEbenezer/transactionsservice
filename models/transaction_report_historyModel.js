const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const transactionReportHistorySchema = new Schema({

    Type: {type: String, required: true},
    userId: {type: String, required: true}, // to be decoded from JWT
    Records: {type: Schema.Types.ObjectId, ref: 'transactionReportModel' }, // should be populated by all the fields in the transaction report model.

    TransactionReportDate: {type: Date, default: Date.now}
});

module.exports = mongoose.model('transactionReportHistoryModel', transactionReportHistorySchema);