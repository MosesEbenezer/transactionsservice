const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const transactionChannelSchema = new Schema({

    Name: {type: String, required: true},
    Enabled: {type: Boolean, required: true},
    ChannelCode: {type: String, required: true},
    
    Created: {type: Date, default: Date.now}
});


module.exports = mongoose.model('transactionChannelModel', transactionChannelSchema);