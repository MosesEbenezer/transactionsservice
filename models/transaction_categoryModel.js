const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const transactionCategorySchema = new Schema({

    Name: {type: String, required: true},
    Enabled: {type: Boolean,  required: true},
    
    Created: {type: Date, default: Date.now}
});


module.exports = mongoose.model('transactionCategoryModel', transactionCategorySchema);