const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const transactionCommissionSchema = new Schema({

    TransactionId: {type: Schema.Types.ObjectId, ref: 'transactionModel', required: true},
    CommissionValue: {type: Number, required: true},
    CommissionProfile: {type: Schema.Types.ObjectId, ref: 'commissionProfileModel', required: true}, // to be populated with all the fields in the commission profile table.

    Created: {type: Date, default: Date.now}
});

module.exports = mongoose.model('transactionCommissionModel', transactionCommissionSchema);