const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const logsSchema = new Schema({

    Name: {type: String, required: true},
    
    loggedOn: {type: Date, default: Date.now}
});

module.exports = mongoose.model('transactionCategoryModel', logsSchema);