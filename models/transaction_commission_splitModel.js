const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const transactionCommissionSplitSchema = new Schema({

    TransactionCommission: {type: String, required: true}, 
    Value: {type: Number, required: true},
    BenefitiaryToBeCredited: {type: String,  required: true}, //to be decoded from jwt
    TransactionId: {type: Schema.Types.ObjectId, ref: 'transactionModel'}, 
    IsValueGiven: {type: Boolean, required: true},

    Created: {type: Date, default: Date.now}
});

module.exports = mongoose.model('transactionCommissionSplitModel', transactionCommissionSplitSchema);