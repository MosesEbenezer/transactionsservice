const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const commissionShareSchema = new Schema({

    BeneficiaryId: {type: String, required: true}, //would be decoded from jwt
    Value: {type: Number, required: true},
    Type: {type: String, required: true},

    Created: {type: Date, default: Date.now}
});

module.exports = mongoose.model('commissionShareModel', commissionShareSchema);