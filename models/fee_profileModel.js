const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const feeProfileSchema = new Schema({

    ServiceCode: {type: String, required: true},
    FeeType: {type: String, required: true},
    FeeValue: {type: Number, required: true},
    FeeCap: {type: Number, required: true},
    HasRange: {type: Boolean, required: true},
    MinAmount: {type: Number, required: true},
    MaxAmount: {type: Number, required: true},
    IncludeVat: {type: Boolean, required: true},

    Created: {type: Date, default: Date.now}
});

module.exports = mongoose.model('feeProfileModel', feeProfileSchema);