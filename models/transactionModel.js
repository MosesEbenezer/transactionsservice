const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const transactionSchema = new Schema({
    UserId: {type: String, required: true},
    ChannelId: {type: Schema.Types.ObjectId, ref: 'transactionChannelModel', required: true},
    TransactionCategory: {type: Schema.Types.ObjectId, ref: 'transactionCategoryModel', required: true},
    TransactionRef: {type: String, required: true},
    DebitedAccount: {type: String, required: true},
    TransactionType: {type: String, required: true},
    TransactionStatus: {type: Schema.Types.ObjectId, ref: 'transactionStatusModel', required: true},
    IP: {type: String, required: true},
    Location: {type: String, required: true},
    TransactionAmount: {type: Number, required: true},
    Narration: {type: String, required: true},
    ParentId: {type: String}, //can be null
    
    TransactionDate: {type: Date, default: Date.now}
});


module.exports = mongoose.model('transactionModel', transactionSchema);