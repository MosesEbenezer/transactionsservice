const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const commissionProfileSchema = new Schema({

    Title: {type: String, required: true}, 
    ServiceCode: {type: String, required: true},
    Amount: {type: Number, required: true}, 

    Created: {type: Date, default: Date.now}
});

module.exports = mongoose.model('commissionProfileModel', commissionProfileSchema);