const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const transactionLimitSchema = new Schema({

    userType: {type: String, required: true},
    
    Created: {type: Date, default: Date.now}
});

module.exports = mongoose.model('transactionLimitModel', transactionLimitSchema);