const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const transactionReportSchema = new Schema({
    DailyCount: {type: Number, required: true},
    DailyValueSum: {type: Number, required: true},
    WeeklyCount: {type: Number, required: true},
    WeeklyValueSum: {type: Number, required: true},
    MonthlyCount: {type: Number, required: true},
    MonthlyValueSum: {type: Number, required: true},
    YearlyCount: {type: Number, required: true},
    YearlyValueCount: {type: Number, required: true},
    AllTimeCount: {type: Number, required: true},
    UserID: {type: String, required: true}, // to be decoded through jwt
    
    Created: {type: Date, default: Date.now}
});


module.exports = mongoose.model('transactionReportModel', transactionReportSchema);