const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const transactionStatusSchema = new Schema({

    Name: {type: String, required: true},
    
    Created: {type: Date, default: Date.now}
});


module.exports = mongoose.model('transactionStatusModel', transactionStatusSchema);