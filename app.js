const express = require('express');
const morgan = require('morgan');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const app = express();

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: false}));
app.use(morgan('dev'));
app.use(cors());

//adding the routes
const mainRoutes = require('./routes/transactions');

app.use('/api/transactions', mainRoutes);

const atlasUrl = process.env.atlasUrl;

mongoose.connect(atlasUrl, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
    if(err) {
        console.log('Not connected to the database: ' + err);
        
    } else {
        console.log('Successfully connected to the database');
        
    }
})


app.listen(process.env.PORT || 3000, (err) => {
    console.log('Server is up and running');
});