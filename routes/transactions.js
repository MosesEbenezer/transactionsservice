const router = require('express').Router();

// require your models here
const CommissionProfileModel = require('../models/commission_profileModel');
const CommissionShareModel = require('../models/commission_shareModel');
const FeeProfileModel = require('../models/fee_profileModel');
const TransactionCategoryModel = require('../models/transaction_categoryModel');
const TransactionChannelModel = require('../models/transaction_channelModel');
const TransactionCommissionSplitModel = require('../models/transaction_commission_splitModel');
const TransactionCommissionModel = require('../models/transaction_commissionModel');
const TransactionReportHistoryModel = require('../models/transaction_report_historyModel');
const TransactionReportModel = require('../models/transaction_reportModel');
const TransactionStatusModel = require('../models/transaction_statusModel');
const TransactionModel = require('../models/transactionModel');


const jwt = require('jsonwebtoken');

const checkJWT = require('../middlewares/check-jwt');

const mongoose = require('mongoose');

//log a transaction
router.post('/log', checkJWT, (req, res, next) => {

        const txnRef = 'GTP' + (Math.floor(Math.random() * 100000))

        let transaction = new TransactionModel();

        transaction.UserId = req.body.user_id;
        transaction.ChannelId = "";
        transaction.TransactionCategory = "";
        transaction.TransactionRef = txnRef;
        transaction.DebitedAccount = req.body.account;
        transaction.TransactionType = req.body.transaction_type;
        transaction.TransactionStatus = "";
        transaction.IP = "";
        transaction.Location = "";
        transaction.TransactionAmount = req.body.transaction_amount;
        transaction.Narration = "";
        transaction.ParentId = ""; //linking to the original transaction that necesitated it.

        TransactionModel.findOne({
            TransactionRef: txnRef
        }, (err, existingTxn) => {
            if (existingTxn) {
                res.status(USM500).json({
                    success: false,
                    message: 'Transaction already saved'
                })
            } else {
                transaction.save();

                //Add Transaction Report
                let transaction_report = new TransactionReportModel;

                transaction_report.DailyCount = "";
                transaction_report.DailyValueSum = "";
                transaction_report.WeeklyCount = "";
                transaction_report.WeeklyCount = "";
                transaction_report.WeeklyValueSum = "";
                transaction_report.MonthlyCount = "";
                transaction_report.MonthlyValueSum = "";
                transaction_report.YearlyCount = "";
                transaction_report.YearlyValueSum = "";
                transaction_report.AllTimeCount = "";
                transaction_report.AllValueValueSum = "";
                transaction_report.UserId = req.body.user_id;

                //some logic to be added here
                transaction_report.save();

                res.status(USM001).json({
                    success: true,
                    message: 'Transaction Successful',
                    transaction
                });
            }
        })
    })

    // get all transactions - transactions history.
    router.get('/get_history', (req, res, next) => {
        TransactionModel.find({}, (err, transactions) => {
            let transactionsArr = [];
            if(err) {
                return res.status(500).send({message: err.message});
            }
            if (BranchBusiness) {
                transactions.forEach(transactions => {
                    transactionsArr.push(transactions)
                });
            }
            res.send(transactions)
        })
    })

module.exports = router;